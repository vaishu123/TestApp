﻿// Create a text file and save data
$(function () {
    $('#saveBtn').click(function () {
        try {
            var checkedRowsData = [];
            var count = 0;
            // Verify Selecting Proper Option of listing Data
            $checkId = $('#mainDiv div').find('input[type="radio"]:checked');
            if ($checkId.attr('id') != "") {
                $("#innerDiv div div[id]").each(function () {
                    divId = $(this).attr('id');
                    $checkBoxSelection = divId != "" ? $('#' + divId).find('input[type="checkbox"]:checked') : null;
                    checkBoxId = $checkBoxSelection.attr('id');
                    $radioSelection = checkBoxId != "" ? $('#' + divId).find('input[type="radio"]:checked') : null;
                    radioId = $radioSelection.attr('id')
                    if (checkBoxId != null && radioId != null) {
                        var keyOption = document.getElementById(checkBoxId).parentElement.innerText
                        var valueOption = document.getElementById(radioId).parentElement.innerText;
                        checkedRowsData.push(keyOption + " = " + valueOption + "\n");
                        count++;
                    }
                });
                if (count == 0) {
                    throw "Please Select at least one option";
                }
            }

            // Verify table row data
            $('#table tr').filter(':has(:checkbox:checked)').each(function () {
                $tr = $(this).find("td");
                var repeat = "";
                var delay = "";
                $td = $tr.find("input:text");
                for (var i = 0; i < $td.length; i++) {
                    var inputId = $td.eq(i).attr('id');
                    var getValue = document.getElementById(inputId).value;
                    if (inputId.match("repeatTxt")) {
                        repeat = document.getElementById(inputId).value;
                    }
                    else {
                        delay = document.getElementById(inputId).value;
                    }
                }
                if (repeat != "" && delay != "") {
                    checkedRowsData.push($tr.eq(1).html() + "\n")
                    checkedRowsData.push("Repeat = " + repeat + "\n");
                    checkedRowsData.push("Delay = " + delay + "\n");
                }
                else {
                    $checkBox = $tr.find('input[type="checkbox"]');
                    checkBoxId = $checkBox.attr('id');
                    alert("please enter the value of repeat & delay");
                    document.getElementById(checkBoxId).checked = false;
                }
            });

            // Get current time and date
            var currentDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
            var currentTime = new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds();
            checkedRowsData.push("Date = " + currentDate + "\n");
            checkedRowsData.push("Time = " + currentTime + "\n");

            // Create and Save data in Text File
            var blob = new Blob(checkedRowsData, { type: "text/plain;charset=utf-8" });
            saveAs(blob, "datafile.txt");
        }
        catch (err) {
            alert(err);
        }
    });
});

// Verify that enter only numeric value in input box
function isNumberInput(event) {
    var isKeyCode = (event.which) ? event.which : event.keyCode
    if (isKeyCode != 46 && isKeyCode > 31 && (isKeyCode < 48 || isKeyCode > 57))
        return false;

    return true;
}

//Show and hide list according to option selection and store it
function showHideListData() {
    if (document.getElementById("systemDVT").checked == true) {
        $('#subDiv1').find('input').prop('checked', false);
        $('#subDiv2').find('input').prop('checked', false);
        subDiv4.style.display = subDiv3.style.display = "block";
        subDiv1.style.display = subDiv2.style.display = "none";
    }
    else if (document.getElementById("softwareTesting").checked == true) {
        $('#subDiv3').find('input').prop('checked', false);
        $('#subDiv4').find('input').prop('checked', false);
        subDiv4.style.display = subDiv3.style.display = "none";
        subDiv1.style.display = subDiv2.style.display = "block";
    }
    else {
        $('#subDiv1').find('input').prop('checked', false);
        $('#subDiv2').find('input').prop('checked', false);
        $('#subDiv3').find('input').prop('checked', false);
        $('#subDiv4').find('input').prop('checked', false);
        subDiv4.style.display = subDiv3.style.display = subDiv1.style.display = subDiv2.style.display = "block";
    }
}

// Enable and disable functionalty of radio buttons
$(function () {
    $(document).on('change', 'input[name="optionsCheckboxes"]', function (e) {
        checkId = $(this).attr('id');
        var parentId = document.getElementById(checkId).parentElement.parentElement.parentElement.id;
        if (document.getElementById(checkId).checked == true) {

            $('#' + parentId).find('input[type="radio"]').prop('disabled', false);
        }
        else {
            $('#' + parentId).find('input[type="radio"]').prop('checked', false);
            $('#' + parentId).find('input[type="radio"]').prop('disabled', true);

        }
    });
});